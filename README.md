# ROS2 Arbiter
A generic arbiter node.
In short, several nodes may publish to the same topic, i.e. `/foo`, yet only one should be allowed to control the topic.
The nodes publishing to the topic, should publish to one of the arbiter inputs.
The arbiter pushes all requests to the original topic based on the priority of each node.

```mermaid
graph LR;
    /foo/_0 & /foo/_1 & /foo/_n --> Arbiter --> /foo
```

## Parameters

| Name           | Type                             | Description                                                                                        | Default |
| :------------- | :------------------------------- | :------------------------------------------------------------------------------------------------- | :------ |
| topic_name     | String                           | Name of the topic to which the arbiter publishes                                                   | None    |
| update_rate    | Number                           | Update rate of the arbiter in Hz                                                                   | 10.0    |
| interfaces     | [Int \| Int[]]                   | A number of input interfaces, or a list of numeric input identifiers                               | 1       |
| prioritization | string [lowest, highest, random] | The prioritization order, i.e. higher/lower numbers have priority, or inputs are randomly selected | lowest  |

> **NOTE**: The arbiter will wait till `topic_name` exists, as this topic should be the manifest for all child topics.
