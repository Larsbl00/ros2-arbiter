from setuptools import setup

package_name = 'ros2_arbiter'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Bloemers, Lars',
    maintainer_email='larsbloemers@gmail.com',
    description='A generic arbiter node',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'arbiter_node = ros2_arbiter.arbiter_node:main',
        ],
    },
)
