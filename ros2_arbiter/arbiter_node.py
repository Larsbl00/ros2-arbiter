"""Creates an arbiter node.

Creates a node with different interfaces for the same topic
all with a different priority.
The highest priority message will overrule the lower priority messages.
"""
import threading
from typing import Any, Dict, List, Optional

import rclpy
import rclpy.node
import rclpy.parameter
from rclpy.parameter import Parameter
from ros2topic.api import get_msg_class

from .selectors import select


class ArbiterNode(rclpy.node.Node):
    """Class representing the Node."""

    ALLOWED_INTERFACES_TYPES = [
        Parameter.Type.INTEGER,
        Parameter.Type.INTEGER_ARRAY
    ]

    def __init__(self) -> None:
        """Initialize the arbiter node."""
        super().__init__('arbiter_node')

        self.declare_parameters(
            namespace='',
            parameters=[
                ('topic_name', None),
                ('update_rate', 10.0),
                ('interfaces', 1),
                ('prioritization', 'lowest')
            ]
        )

        self.interfaces = self.get_parameter('interfaces')
        self.prioritization = self.get_parameter('prioritization')
        self.topic_name = self.get_parameter('topic_name')
        self.msg_type = get_msg_class(self,
                                      self.topic_name.value,
                                      blocking=True)

        self.update_rate = self.get_parameter('update_rate')

        self.get_logger().info(
            f'Found topic "{self.topic_name.value}" '
            f'with type: "{self.msg_type}"')

        self._reset_buffer()

        self._create_subscriptions()
        self._create_publishers()

    def update(self) -> None:
        """Update the node."""
        self._handle_buffer()
        self._reset_buffer()

    def _arbiter_callback(self, index: int, value: Any) -> None:
        """Handle a callback for arbiter callbacks.

        Assigns the buffer at `index` to `value`.
        """
        self.buffer[index] = value

    def _create_subscriptions(self) -> None:
        """Create the topics of the node."""
        self._arbiter_subs = []

        for i in self.buffer.keys():
            self._arbiter_subs.append(self.create_subscription(
                self.msg_type,
                f'{self.topic_name.value}/_{i}',
                lambda msg, i=i: self._arbiter_callback(i, msg),
                20
            ))

    def _create_publishers(self) -> None:
        """Create the publishers of the node."""
        self.output_publisher = self.create_publisher(
            self.msg_type, self.topic_name.value, 20)

    def _handle_buffer(self) -> None:
        """Handle the buffer and publish the correct message."""
        msgs = {k: v for (k, v) in self.buffer.items() if v is not None}
        if len(msgs) <= 0:
            return

        self.get_logger().debug(
            f'Selecting one of {msgs=} with '
            f'{self.prioritization.value=}'
        )

        self.output_publisher.publish(
            select(self.prioritization.value, msgs)
        )

    def _reset_buffer(self) -> None:
        """Reset the buffer."""
        self.buffer: Dict[int, Optional[Any]] = {
            n: None for n in self.interfaces
        }

    @property
    def interfaces(self) -> List[int]:
        """List of integers that represent the interface IDs."""
        return self.__interfaces

    @interfaces.setter
    def interfaces(self, val: Parameter) -> None:
        if val.type_ not in ArbiterNode.ALLOWED_INTERFACES_TYPES:
            raise LookupError(
                f'Make sure "interfaces" is either '
                f'{ArbiterNode.ALLOWED_INTERFACES_TYPES}, not {val.type_}'
            )

        self.__interfaces = val.value \
            if val.type_ is Parameter.Type.INTEGER_ARRAY \
            else range(val.value)


def main(args=None) -> None:
    """Entry point for the node."""
    rclpy.init(args=args)

    node = ArbiterNode()
    # NOTE Run node spin on a second thread, as explained in
    # <https://answers.ros.org/question/358343/rate-and-sleep-function-in-rclpy-library-for-ros2/?answer=358386#post-id-358386>
    thread = threading.Thread(target=rclpy.spin, args=(node, ), daemon=True)
    thread.start()

    rate = node.create_rate(node.get_parameter_or('update_rate').value)

    while rclpy.ok():
        node.update()
        rate.sleep()

    rclpy.shutdown()
    thread.join()


if __name__ == '__main__':
    main()
