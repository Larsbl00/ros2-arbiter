"""Contains all selection methods."""

from typing import Any, Dict
import random


def highest(items: Dict[int, Any]) -> Any:
    """Select an item based on a high-value priority.

    Args:
        items (Dict[int, Any]): A dict where the key represents the priority
        of the value

    Returns:
        Any: The value of the *highest* key

    """
    return items[max(items.keys())]


def lowest(items: Dict[int, Any]) -> Any:
    """Select an item based on a low-value priority.

    Args:
        items (Dict[int, Any]): A dict where the key represents the priority
        of the value

    Returns:
        Any: The value of the *lowest* key

    """
    return items[min(items.keys())]


def rand(items: Dict[int, Any]) -> Any:
    """Select an item for arbitrary reasons.

    Args:
        items (Dict[int, Any]): A dict where the key represents the priority
        of the value

    Returns:
        Any: A random element of `items`

    """
    return random.choice(list(items.values()))


PRIORITIZATION_MODES = {
    'lowest': lowest,
    'highest': highest,
    'random': rand
}


def select(mode: str, items: Dict[int, Any]) -> Any:
    """Select an item based on a give prioritization `mode`.

    Args:
        mode (str): A string representing the prioritization mode,
        see `PRIORITIZATION_MODES`
        items (Dict[int, Any]): A dict where the key represents
        the priority of the value

    Raises:
        KeyError: Given `mode` is not in `PRIORITIZATION_MODES`

    Returns:
        Any: A value selected from `items` based on `mode`.

    """
    if (selector := PRIORITIZATION_MODES.get(mode)) is None:
        raise KeyError(
            f'No selection mode "{mode}" was found, '
            f'please use any of these: {PRIORITIZATION_MODES.keys()}'
        )

    return selector(items)
